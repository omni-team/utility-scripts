#!/usr/bin/env python
import os
from sendingmail import send_gmail
from configs import settings
from pash_utilities import time_stamp, resolve_path, safe_delete, do_zip
from dropbox import dropbox_upload, create_stamp_folder

__author__ = 'kay'


def dump_databases(result_list):
    # Dump From MySQL
    mysql_config = settings['mysql']
    username = mysql_config['username']
    passwd = mysql_config['passwd']

    databases = mysql_config['names']
    if '*' in mysql_config['names']:
        io = os.popen('mysql -u{0} -p{1} mysql -e "show databases"'.format(username, passwd))
        databases = [x.strip() for x in io.readlines()]

    for db in databases:
        try:
            if db not in ['information_schema', 'performance_schema', 'Database', 'mysql']:
                dumped_db = dump_mysql_database(db, username, passwd)
                zip_upload_and_maybe_delete(dumped_db, True, 'mysql')
                result_list.append("SUCCESS:    ->" + db + " dumped")
        except Exception as r:
            result_list.append("ERROR  :    ->" + str(r))

    # Dump From Postgres
    psql_config = settings['psql']
    username = psql_config['username']
    passwd = psql_config['passwd']
    port = psql_config.get('port', '5432')  # Default port is 5432 if not specified
    databases = psql_config['names']
    if '*' in psql_config['names']:
        os.environ['PGPASSWORD'] = passwd
        io = os.popen('psql --username={0} --port={1} -c "SELECT datname FROM pg_database"'.format(username, port))
        databases = [x.strip() for x in io.readlines()]
        databases = databases[:-1]

    for db in databases:
        try:
            if db not in ['postgres', 'template0', 'template1', '-----------', 'datname']:
                dumped_db = dump_psql_database(db, username, passwd, port)
                zip_upload_and_maybe_delete(dumped_db, True, 'psql')
                result_list.append("SUCCESS:    ->" + db + " dumped")
        except Exception as r:
            result_list.append("ERROR  :    ->" + str(r))

    # Dump From MongoDB
    mongo_config = settings['mongo']
    container_name = mongo_config['container_name']
    mongo_port = mongo_config['port']
    mongo_username = mongo_config['username']
    password = mongo_config['password']

    try:
        dumped_db = dump_mongo_database(container_name, mongo_port, mongo_username, password)
        zip_upload_and_maybe_delete(dumped_db, True, 'mongo')
        result_list.append("SUCCESS:    -> MongoDB dumped")
    except Exception as r:
        result_list.append("ERROR  :    ->" + str(r))

    # Dump From Mysql Docker
    mysql_docker_config = settings['mysql_docker']
    mysql_docker_container = mysql_docker_config['container_name']
    mysql_docker_port = mysql_docker_config['port']
    mysql_docker_database = mysql_docker_config['database']
    mysql_docker_username = mysql_docker_config['username']
    mysql_docker_password = mysql_docker_config['password']

    try:
        dumped_db = dump_mysql_docker_db(mysql_docker_container, mysql_docker_port, mysql_docker_database,
                                         mysql_docker_username, mysql_docker_password)
        zip_upload_and_maybe_delete(dumped_db, True, 'mysql_docker')
        result_list.append("SUCCESS:    -> MysqlDB Docker dumped")
    except Exception as r:
        result_list.append("ERROR MysqlDB Docker:    ->" + str(r))


def dump_mysql_database(db, username, passwd):
    db_path = resolve_path('temp/{0}_{1}.sql'.format(time_stamp(), db))
    print("Dumping [" + db + "] to " + db_path)
    dump_cmd = 'mysqldump -u{0} -p{1} {2} > {3}'.format(username, passwd, db, db_path)
    ret_code = os.system(dump_cmd)
    if ret_code == 0:
        print('Dumped       : [' + str(db) + ']')
        return db_path
    else:
        print('Error Dumping: [' + str(db) + ']')
        safe_delete(db_path)
        raise Exception('Error Dumping ' + str(db))


def dump_psql_database(db, username, passwd, port='5432'):
    db_path = resolve_path('temp/{0}_{1}.sql'.format(time_stamp(), db))
    print("Dumping [" + db + "] to " + db_path)
    os.environ['PGPASSWORD'] = passwd
    dump_cmd = 'pg_dump -U{0} -p{1} {2} > {3}'.format(username, port, db, db_path)
    ret_code = os.system(dump_cmd)
    if ret_code == 0:
        print('Dumped       : [' + str(db) + ']')
        return db_path
    else:
        print('Error Dumping: [' + str(db) + ']')
        safe_delete(db_path)
        raise Exception('Error Dumping ' + str(db))


def dump_mongo_database(container_name, mongo_port, mongo_username, password):
    # Dump MongoDB using Docker
    project_directory = settings['project_directory']
    backup_directory = os.path.join(project_directory, 'backups')

    # Create the backup directory if it doesn't exist
    if not os.path.exists(backup_directory):
        os.makedirs(backup_directory)

    new_file_name = time_stamp() + "_" + container_name + "_dump.archive"
    backup_path = os.path.join(backup_directory, new_file_name)

    # Change directory to the project directory
    os.chdir(project_directory)

    # Dump MongoDB using Docker exec
    dump_cmd = 'docker exec {} mongodump --port {} --username {} --password {} --archive={} --gzip'.format(
        container_name, mongo_port, mongo_username, password, '/var/lib/backup.archive')
    ret_code = os.system(dump_cmd)

    if ret_code == 0:
        # Copy dumped file from container to the backup directory
        docker_cp_cmd = 'docker cp {}:{} {}'.format(container_name, '/var/lib/backup.archive', backup_path)
        ret_code_cp = os.system(docker_cp_cmd)

        if ret_code_cp == 0:
            return backup_path
        else:
            raise Exception('Error copying dumped MongoDB file from container to backup directory')
    else:
        raise Exception('Error dumping MongoDB')


def dump_mysql_docker_db(container_name, mysql_port, database, mysql_username, password):
    # Dump Mysql using Docker
    project_directory = settings['project_directory']
    backup_directory = os.path.join(project_directory, 'backups')

    # Create the backup directory if it doesn't exist
    if not os.path.exists(backup_directory):
        os.makedirs(backup_directory)

    new_file_name = time_stamp() + "_" + container_name + "_dump.sql.gz"
    backup_path = os.path.join(backup_directory, new_file_name)

    # Change directory to the project directory
    os.chdir(project_directory)

    # Dump Mysql using Docker exec
    dump_cmd = 'docker exec {} mysqldump -P {} -u {} -p{} {} > {}'.format(
        container_name, mysql_port, mysql_username, password, database,
        backup_directory + '/camunda_db_dump.sql')
    ret_code = os.system(dump_cmd)

    if ret_code == 0:
        # Copy dumped file from container to the backup directory
        docker_cp_cmd = 'cp {} {}'.format(backup_directory + '/camunda_db_dump.sql', backup_path)
        ret_code_cp = os.system(docker_cp_cmd)

        if ret_code_cp == 0:
            return backup_path
        else:
            raise Exception('Error copying dumped mysqlDB file from container to backup directory')
    else:
        raise Exception('Error dumping mysqlDB')


def zip_upload_and_maybe_delete(src_file, delete_original, pre_fix):
    zipped_file = zip_file(src_file, pre_fix)
    if delete_original:
        safe_delete(src_file)
    dropbox_upload(zipped_file, stamp_folder=create_stamp_folder())
    safe_delete(zipped_file)


def zip_all_files(result_list):
    files_ = settings['files']
    for src_file in files_:
        try:
            prefix = time_stamp() + '_FILE_'
            final_file = src_file

            if src_file.__class__ == dict:
                prefix += src_file['prefix']
                final_file = src_file['file']

            zip_upload_and_maybe_delete(final_file, False, prefix)
            result_list.append("SUCCESS:    ->" + final_file + " uploaded with prefix " + prefix)
        except Exception as e:
            result_list.append("ERROR  :    ->" + str(e))


def zip_file(src_path, pre_fix=''):
    if os.path.exists(src_path):
        file_name = os.path.basename(src_path)
        dst_file = resolve_path('temp/' + pre_fix + file_name)
        final_dst_path = do_zip(src_path, dst_file)
        return final_dst_path
    else:
        raise Exception("Error when Zipping: " + src_path + " File doesnt exist")


def create_temp_dir():
    if not os.path.exists(resolve_path('temp')):
        print('creating temp directory %s' % 'temp')
        os.makedirs('temp')


def main():
    result_list = []

    orig = os.environ['NSS_DISABLE_HW_GCM'] if 'NSS_DISABLE_HW_GCM' in os.environ.keys() else None
    try:
        os.environ['NSS_DISABLE_HW_GCM'] = '1'
        create_temp_dir()
        dump_databases(result_list)
        zip_all_files(result_list)
    except Exception as ex:
        result_list.append(str(ex))
    finally:
        send_email_list(result_list)
        if orig is None:
            del os.environ['NSS_DISABLE_HW_GCM']
        else:
            os.environ['NSS_DISABLE_HW_GCM'] = orig


def send_email_list(message_list):
    email_conf = settings['mail_settings']
    sender = email_conf['sender']
    receiver = email_conf['receiver']
    password = email_conf['password']
    report = ''
    end_message = 'SUCCESS'
    for res in message_list:
        report += res + "\n"
        if res.startswith('ERROR'):
            end_message = 'ERRORS'
    print(report)
    send_gmail(sender, password, receiver,
               "Back Up Report: " + time_stamp() + " To " + create_stamp_folder() + " with " + end_message, report)


if __name__ == '__main__':
    main()
