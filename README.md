## INSTRUCTIONS
If you are running this script for the first time, please follow the instructions:

1) Open the following URL in your Browser, and log in using your account: https://www.dropbox.com/developers/apps
2) Click on "Create App", then select "Dropbox API app"
3) Now go on with the configuration, choosing the app permissions and access restrictions to your DropBox folder
4) Enter the "App Name" that you prefer (e.g. MyUploader24372312826910)

Now, click on the "Create App" button.

When your new App is successfully created, please click on the Generate button
under the 'Generated access token' section, then copy the access token.

Run the script ``./dropbox_uploader.sh``
It will ask for the access token, paste the copied access token and you should be good to go.


## CONFIGURING WHICH DATABASES TO BACK UP

Edit the file `configs.py` section 'settings'


### Back up one Database

````python
settings = dict(

    mysql=dict(username='root',
               passwd='pass',
               names=['myDatabaseName']),
)
````

### Back up all Databases

````python
settings = dict(

    mysql=dict(username='root',
               passwd='pass',
               names=['*']),
)
````

### Back up files

````python
settings = dict(
    files=['/path/to/file']
)
````

### Back up all Databases and Some Files

````python
settings = dict(
    mysql=dict(username='root',
               passwd='pass',
               names=['*']),
    files=['/path/to/file']
)
````

### getting authenticated documentation
https://www.dropbox.com/developers/documentation/http/documentation#oauth2-token
