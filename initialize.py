import requests
import os

# Fill in your credentials and refresh token here
refresh_token = "mC4FHn_ZVzgAAAAAAAAAAQRk4EVehGydRbQrMUTBkTVMpVe8EQ7JClD59YvDM9r_"
client_id = "y11ukili1plezjq"
client_secret = "xjin4mcgu0sxob2"

# Define the request parameters
url = "https://api.dropbox.com/oauth2/token"
data = {
    "grant_type": "refresh_token",
    "refresh_token": refresh_token,
    "client_id": client_id,
    "client_secret": client_secret,
}

# Send the POST request
response = requests.post(url, data=data)

# Print the response
if response.status_code == 200:
    access_token = response.json()["access_token"]

    # Check if the dropbox_uploader script exists
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    script_path = os.path.join(ROOT_DIR, "bash", "Dropbox-Uploader-0.16", "dropbox_uploader.sh")
    if not os.path.exists(script_path):
        print('Error: ' + script_path + ' does not exist')
        exit()

    # Run the unlink command
    command = script_path + " unlink"
    input_c = "y"
    os.system("echo '%s' | %s" % (input_c, command))

    # Call the Dropbox upload script with the access token as an argument
    command = script_path
    input_c = access_token + "\ny"
    os.system("echo '%s' | %s" % (input_c, command))

    # Call the Dropbox upload script info
    command = script_path + " info"
    input_c = " "
    os.system("echo '%s' | %s" % (input_c, command))

else:
    print("Error: " + str(response.status_code) + " " + response.text)
    exit()
