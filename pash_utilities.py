import zipfile
import os
import sys
import platform
import datetime

this_dir = os.path.dirname(os.path.abspath(__file__))
root_dir = this_dir


def find_executable(cmd, mode=os.F_OK | os.X_OK, path=None):
    # Compatibility method for shutil.which
    def _access_check(fn, _mode):
        return os.path.exists(fn) and os.access(fn, _mode) and not os.path.isdir(fn)

    if _access_check(cmd, mode):
        return cmd

    path = (path or os.environ.get("PATH", os.defpath)).split(os.pathsep)

    if sys.platform == "win32":
        if os.curdir not in path:
            path.insert(0, os.curdir)
        pathext = os.environ.get("PATHEXT", "").split(os.pathsep)
        matches = [cmd for ext in pathext if cmd.lower().endswith(ext.lower())]
        files = [cmd] if matches else [cmd + ext.lower() for ext in pathext]
    else:
        files = [cmd]

    seen = set()
    for folder in path:
        folder = os.path.normcase(folder)
        if folder not in seen:
            seen.add(folder)
            for the_file in files:
                name = os.path.join(folder, the_file)
                if _access_check(name, mode):
                    return name
    return None


def time_stamp():
    # return 'T'
    # return socket.gethostname() + '-BACKUP'
    # return datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    return datetime.datetime.now().strftime('%Y-%m-%d')


def resolve_path(relative_path):
    return root_dir + '/' + relative_path


def is_windows():
    return platform.system() == 'Windows'


def do_zip(src, dst):
    print("    [Zipping] {0} to {1}.??".format(src, dst))
    if os.path.isfile(src):
        return _try_gzip_or_internal_zip(src, dst)
    else:
        return _try_tar_or_internal_zip_folder(src, dst)


def _try_gzip_or_internal_zip(src, dst):
    new_dst = dst + '.zip'
    if gzip_exits():
        print('    [Zipping With GNU GZIP]')
        new_dst = dst + '.gz'
        safe_delete(new_dst)
        os.system('gzip -c ' + src + ' > ' + new_dst)
    else:
        safe_delete(new_dst)
        zf = zipfile.ZipFile(new_dst, "w")
        zf.write(src, os.path.basename(src), zipfile.ZIP_DEFLATED)
        zf.close()
    return new_dst


def _try_tar_or_internal_zip_folder(src, dst):
    new_dst = dst + '.zip'
    if find_executable('tar') and gzip_exits():
        print('    [Zipping With GNU TAR GZIP]')
        new_dst = os.path.normpath(dst + '.tar.gz')
        ch_dir = os.path.dirname(src)
        file_name = os.path.basename(src)
        safe_delete(new_dst)
        command = 'tar zcf  "' + new_dst + '" -C "' + ch_dir + '" ' + file_name
        print(command)
        os.system(command)
    else:
        abs_src = os.path.abspath(src)
        safe_delete(new_dst)
        zf = zipfile.ZipFile(new_dst, "w", zipfile.ZIP_STORED)
        for dir_name, sub_dirs, files in os.walk(src):
            for filename in files:
                abs_name = os.path.abspath(os.path.join(dir_name, filename))
                arc_name = abs_name[len(abs_src) + 1:]
                zf.write(abs_name, arc_name)
        zf.close()
    return new_dst


def gzip_exits():
    gzip = find_executable('gzip')
    return gzip is not None


def safe_delete(fd):
    if os.path.exists(fd):
        print('cleaning... {0}'.format(fd))
        os.remove(fd)
