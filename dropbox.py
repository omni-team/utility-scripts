import os
import socket
import datetime

import pash_utilities as pu

__author__ = 'kay'


def dropbox_upload(src_file, dst='Omnitech/server-backups', stamp_folder=''):
    final_dst = os.path.join(dst, stamp_folder, os.path.basename(src_file))
    if pu.is_windows():
        return _win_upload(src_file, final_dst)
    else:
        return _shell_upload(src_file, final_dst)


def create_stamp_folder():
    return socket.gethostname() + datetime.datetime.now().strftime('/%Y-%m/%d/')


def _shell_upload(src_file, dst):
    upload_command = _create_linux_cmd('upload', src_file, dst)
    exit_code = os.system(upload_command)
    if exit_code == 0:
        return exit_code == 0
    else:
        raise Exception("Error when uploading " + src_file)


def _win_upload(src_file, dst):
    upload_command = _create_win_cmd('--upload', src_file, dst)
    exit_code = _win_exec(upload_command)
    if exit_code == 200:
        return True
    else:
        raise Exception("Error when uploading " + src_file)


def _create_win_cmd(*args):
    args_string = ' '.join(args)
    return pu.resolve_path('binary/dropbox/dropbox.exe ' + args_string)


def _create_linux_cmd(*args):
    args_string = ' '.join(args)

    path = pu.resolve_path(
        'bash/Dropbox-Uploader-0.16/dropbox_uploader.sh -p -h '
        '-f /root/.dropbox_uploader ' + args_string)
    # 'bash/Dropbox-Uploader-0.16/home/marthamareal/.dropbox_uploader.sh ' + args_string)
    return path


def _win_exec(upload_command):
    exit_code = _exec_in_folder('binary/dropbox/', lambda: os.system(upload_command))
    return exit_code


def _exec_in_folder(folder, func):
    cwd = os.getcwd()
    os.chdir(pu.resolve_path(folder))
    try:
        return func()
    finally:
        os.chdir(cwd)
