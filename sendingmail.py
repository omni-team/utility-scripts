def send_gmail(user, pwd, recipient, subject, body):
    import smtplib

    # to work, make sure in your email (both sender and reciever) settings All access to less secure apps is on
    gmail_user = user
    gmail_pwd = pwd
    from_user = user
    to_email = recipient if type(recipient) is list else [recipient]
    email_subject = subject
    email_body = body

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (from_user, ", ".join(to_email), email_subject, email_body)
    try:

        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(from_user, to_email, message)
        server.close()
        print ('successfully sent the mail')
    except Exception as e:
        print ("failed to send mail")
	print (e)
